# AS OF MAY 2018
# gcc:6 is Debian 8 (Jessie) with gcc 6.5.0 glibc 2.19 cmake 3.0.2
# gcc:7 is Debian 9 (Stretch) with gcc 7.4.0 glibc 2.24 cmake 3.7.2
# gcc:8 is Debian 9 (Stretch) with gcc 8.3.0 glibc 2.24 cmake 3.7.2

# default version is 6
ARG gcc_version=6

FROM gcc:${gcc_version}

RUN apt-get update && apt-get -y upgrade

VOLUME /home/build/dtn2_package

# DTN2
# ARG default values: (versions have not changed since 2012)
ARG dtn2_version=2.9.0
ARG oasys_version=1.6.0
ARG parallelism=1

# Install dependencies
RUN apt-get install -y tcl8.5-dev libdb5.3-dev

# Download sources
WORKDIR /home/build/
RUN wget "http://downloads.sourceforge.net/project/dtn/oasys/oasys-$oasys_version/oasys-$oasys_version.tgz"
RUN wget "http://downloads.sourceforge.net/project/dtn/DTN2/dtn-$dtn2_version/dtn-$dtn2_version.tgz"
RUN mkdir -p dtn2 && tar xvaf "dtn-$dtn2_version.tgz" -C dtn2 --strip-components=1
RUN mkdir -p oasys && tar xvaf "oasys-$oasys_version.tgz" -C oasys --strip-components=1

# Compile oasys
WORKDIR /home/build/oasys
# force newer compilers support
RUN sed -i -e 's/3.*|4.*)/3.*|4.*|5.*|6.*|7.*|8.*)/' configure
# compilation error if -j is used
RUN ./configure --with-extra-cflags=-std=c++03 && make

# Compile DTN2
WORKDIR /home/build/dtn2
# force newer compilers support
RUN sed -i -e 's/3.*|4.*)/3.*|4.*|5.*|6.*|7.*|8.*)/' configure
RUN ./configure --prefix=/usr/local/ --disable-ecl --disable-edp --with-extra-cflags=-std=c++03 && make -j${parallelism}
# install to system folder for dtnperf compilation on this container
RUN make install && ldconfig
RUN INCLUDE_DIR=$(dirname $(find /usr -name dtnd)|sed s/bin$/include/) && \
	echo $INCLUDE_DIR; \
	install -d $INCLUDE_DIR $INCLUDE_DIR/oasys; \
	install dtn-config.h applib/*.h $INCLUDE_DIR; \
	install oasys/include/oasys/oasys-config.h $INCLUDE_DIR/oasys

# install to a separate folder to prepare package
# if VOLUME is mounted, the package will be available in the host fs
CMD DESTDIR=/home/build/dtn2_package make install; \
	INCLUDE_DIR=$(dirname $(find /home/build/dtn2_package -name dtnd)|sed s/bin$/include/) && \
	echo $INCLUDE_DIR; \
	install -d $INCLUDE_DIR $INCLUDE_DIR/oasys; \
	install dtn-config.h applib/*.h $INCLUDE_DIR; \
	install oasys/include/oasys/oasys-config.h $INCLUDE_DIR/oasys

